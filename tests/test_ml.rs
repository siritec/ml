extern crate ml;
extern crate ndarray;
extern crate ndarray_linalg;
extern crate rand;

pub use rand::Rng;
pub use ndarray::prelude::*;
pub use self::ndarray_linalg::*;

pub use ml::multi_reg::*;

#[test]
pub fn test_reg() {
    let y: Array2<f64> = random((200, 30));
    let x: Array2<f64> = random((200, 40));

    let mut b: Array2<f64> = Array::zeros((40, 30));
    let mut r: Array2<f64> = Array::zeros((200, 30));
    let mut p: Array2<f64> = Array::zeros((200, 30));
    
    fit_vec(&y, &x, &mut b, &mut r, &mut p);

    assert_eq!(1, 1);
}


