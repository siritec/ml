pub use ndarray::*;
use std::ptr;

pub trait ArrayExt<S: Data, D: Dimension> {
    fn filter_axis<F>(&self, axis: Axis, f: F) -> ArrayBase<OwnedRepr<S::Elem>, D>
        where F: Fn(ArrayView<S::Elem, D::Smaller>) -> bool;
}

pub fn to_contiguous<'a, B: 'a, D>(iter: iter::Iter<'a, B, D>, dim: D) -> ArrayBase<OwnedRepr<B>, D>
    where B: Clone,
          D: Dimension,
{
    let (size, _) = iter.size_hint();
    let mut result = Vec::with_capacity(size);
    let mut out_ptr = result.as_mut_ptr();
    let mut len = 0;
    iter.fold((), |(), elt| {
        unsafe {
            ptr::copy_nonoverlapping(elt, out_ptr, 1);
            len += 1;
            out_ptr = out_ptr.offset(1);
        }
    });

    debug_assert!(len == size);

    unsafe {
        result.set_len(len);
        ArrayBase::from_shape_vec_unchecked(dim, result)
    }
}

impl<S> ArrayExt<S, Ix2> for ArrayBase<S, Ix2>
where S: Data,
      S::Elem: Clone,
{
    fn filter_axis<F>(&self, axis: Axis, f: F) -> ArrayBase<OwnedRepr<S::Elem>, Ix2>
    where F: Fn(ArrayView<S::Elem, Ix1>) -> bool
    {
        let (r, c) = self.dim();

        let mut v: Vec<S::Elem> = Vec::with_capacity(self.len());
        let mut pw = v.as_mut_ptr();

        let mut l = 0;
        let mut taken;

        let new_shape: Shape<Ix2> = match (self.is_standard_layout(), axis.index()) {
            (true, 0) => {
                for row in self.genrows() {
                    taken = f(row);
                    if taken {
                        unsafe {
                            ptr::copy_nonoverlapping(row.as_ptr(), pw, c);
                            pw = pw.offset(c as isize);
                        }
                        l += c;
                    } 
                }
                (l / c, c).into_shape()
            },
            (false, 0) => {
                let mut temp = to_contiguous(self.view().iter(),
                        (r, c).into_dimension());

                for col in temp.genrows() {
                    taken = f(col);
                    if taken {
                        unsafe {
                            ptr::copy_nonoverlapping(col.as_ptr(), pw, c);
                            pw = pw.offset(c as isize);
                        }
                        l += c;
                    } 
                }
                (l / c, c).into_shape()
            }
            (_, 1) => {
                let mut temp;

                for col in self.gencolumns() {
                    taken = f(col);
                    if taken {
                        let p = if col.is_standard_layout() {
                            col.as_ptr()
                        } else {
                            temp = col.to_owned();
                            temp.as_ptr()
                        };

                        unsafe {
                            ptr::copy_nonoverlapping(p, pw, r);
                            pw = pw.offset(r as isize);
                        }
                        l += r;
                    }
                }
                (r, l / r).into_shape().f()
            },
            _ => unreachable!(),
        };

        unsafe {
            v.set_len(l);
            ArrayBase::from_shape_vec_unchecked(new_shape, v)
        }
    }
}

macro_rules! impl_ext (
    ($($size:expr),+) => {
        $(
            impl<S> ArrayExt<S, Dim<[usize; $size]>> for ArrayBase<S, Dim<[usize; $size]>>
            where S: Data,
                  S::Elem: Clone,
            {
                fn filter_axis<F>(&self, axis: Axis, f: F) -> ArrayBase<OwnedRepr<S::Elem>, Dim<[usize; $size]>>
                where F: Fn(ArrayView<S::Elem, Dim<[usize; $size - 1]>>) -> bool
                {
                    let raw_shape = self.shape();
                    let a = axis.index();
                    let al = self.len_of(axis);

                    assert!(a < raw_shape.len(), "Axis out of bounds");

                    let mut temp_shape = [0; $size];
                    let mut re_permute = [0; $size];

                    for i in 0..$size {
                        if (i > 0) && (i <= a) {
                            temp_shape[i] = raw_shape[i - 1]; 
                        } else {
                            temp_shape[i] = i;
                        }

                        if i < a {
                            re_permute[i] = i + 1;
                        } else if i == a {
                            re_permute[i] = 0;
                        } else {
                            re_permute[i] = i;
                        }
                    }

                    let mut l = 0;
                    let c = self.len() / al;

                    let mut data = Vec::with_capacity(self.len());
                    let mut pw = data.as_mut_ptr();

                    let mut temp;
                    let mut taken;

                    for row in self.axis_iter(axis) {
                        taken = f(row);
                        if taken {
                            let p = if row.is_standard_layout() {
                                row.as_ptr()
                            } else {
                                temp = row.to_owned();
                                temp.as_ptr()
                            };

                            unsafe {
                                ptr::copy_nonoverlapping(p, pw, c);
                                pw = pw.offset(c as isize);
                            }
                            l += c;
                        }
                    }

                    temp_shape[0] = l / c;

                    let x = unsafe {
                        data.set_len(l);
                        ArrayBase::from_shape_vec_unchecked(temp_shape, data)
                    };

                    x.permuted_axes(re_permute)
                }
            }
        )+
    }
);

impl_ext!(3, 4, 5, 6);
