use ::array::*;

use rand::rngs::SmallRng;
use rand::{thread_rng, FromEntropy};
use rand::distributions::Distribution;

pub fn random<Sh, Dist, S, D>(shape: Sh, dist: Dist) -> ArrayBase<S, D>
where S: DataOwned,
      D: Dimension,
      Dist: Distribution<S::Elem>,
      Sh: ShapeBuilder<Dim=D>
{
    let mut expensive = thread_rng();
    ArrayBase::from_shape_fn(shape, |_| dist.sample(&mut expensive))
}


pub fn random_fast<Sh, Dist, S, D>(shape: Sh, dist: Dist) -> ArrayBase<S, D>
where S: DataOwned,
      D: Dimension,
      Dist: Distribution<S::Elem>,
      Sh: ShapeBuilder<Dim=D>
{
    let mut small = SmallRng::from_entropy();
    ArrayBase::from_shape_fn(shape, |_| dist.sample(&mut small))
}

