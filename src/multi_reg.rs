use std::f64::NAN;

use ::array::prelude::*;
use ::array::linalg::{general_mat_mul, general_mat_vec_mul};
use ndarray_linalg::solve::InverseInto;
use ndarray_linalg::error::*;

// Would be good if the inverse matrix operation could be in-place.
pub fn var_cov(x: &Array2<f64>) -> Result<Array2<f64>> {
    let (_, a) = x.dim();
    let mut c = unsafe { Array2::uninitialized((a, a)) };
    general_mat_mul(1.0, &x.t(), x, 0., &mut c);
    c.inv_into()
}


pub fn hat(_x: &Array2<f64>) -> Array2<f64> {
    unimplemented!()
}


pub fn fit(y: &Array1<f64>, x: &Array2<f64>, beta: &mut Array1<f64>, e: &mut Array1<f64>, p: &mut Array1<f64>) {

    let (l, (m, b), n, s, t) = (y.dim(), x.dim(), beta.dim(), e.dim(), p.dim());
    if (l != m) || (b != n) || (l != s) || (l != t) {
        panic!("Wrong shapes of input matrix")
    }

    let vc = var_cov(x).unwrap();
    let mut temp = unsafe { Array1::uninitialized(b) };

    general_mat_vec_mul(1.0, &x.t(), y, 0., &mut temp);

    // Compute beta
    general_mat_vec_mul(1.0, &vc, &temp, 0., beta);

    // Compute prediction
    general_mat_vec_mul(1.0, x, beta, 0., p);

    // Compute error
    e.assign(&(y - &*p));
} 


pub fn fit_vec(y: &Array2<f64>, x: &Array2<f64>, beta: &mut Array2<f64>, e: &mut Array2<f64>, p: &mut Array2<f64>) {

    let ((l, a), (m, b), (n, c), (s, g), (t, h)) = (y.dim(), x.dim(), beta.dim(), e.dim(), p.dim());

    if (l != m) || (b != n) || (a != c) || (l != s) || (l != t) || (a != g) || (a != h) {
        panic!("Wrong shapes of input matrix")
    }

    let vc = var_cov(x).unwrap();
    let mut temp = unsafe { Array2::uninitialized((b, a)) };

    general_mat_mul(1.0, &x.t(), y, 0., &mut temp);

    // Compute beta
    general_mat_mul(1.0, &vc, &temp, 0., beta);

    // Compute prediction
    general_mat_mul(1.0, x, beta, 0., p);

    // Compute error
    e.assign(&y);
    *e -= &*p;
} 


pub struct OLS {
    endog: Array2<f64>,
    exog: Array2<f64>,
    beta: Array2<f64>,
    error: Array2<f64>,
    pred: Array2<f64>,
}


impl OLS {
    pub fn new(y: Array2<f64>, x: Array2<f64>) -> Self {
        let ((l, a), (m, b)) = (y.dim(), x.dim());

        assert!(l == m, "Wrong shapes of input matrix");

        let is_valid = y.genrows().into_iter()
            .zip(x.genrows().into_iter())
            .enumerate()
            .filter_map(|(i, (ry, rx))| {
                if ry.iter().all(|e| e.is_finite()) && rx.iter().all(|e| e.is_finite()) {
                    Some(i)
                } else {
                    None
                }
            }).collect::<Vec<_>>();

        let b: Array2<f64> = Array::from_elem((b, a), NAN);
        let e: Array2<f64> = Array::from_elem((is_valid.len(), a), NAN);
        let p: Array2<f64> = Array::from_elem((is_valid.len(), a), NAN);

        OLS {
            endog: y.select(Axis(0), &is_valid),
            exog: x.select(Axis(0), &is_valid),
            beta: b,
            error: e,
            pred: p,
        }
    }

    pub fn fit(&mut self) -> &Self {
        fit_vec(&self.endog, &self.exog, &mut self.beta, &mut self.error, &mut self.pred);
        self
    }
}
