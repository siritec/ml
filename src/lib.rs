extern crate ndarray;
extern crate ndarray_linalg;
extern crate rand;

pub mod multi_reg;
pub mod array;
pub mod proba;

